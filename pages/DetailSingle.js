import MapIcon from '@material-ui/icons/Map';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar'
import PublicIcon from '@material-ui/icons/Public'
import DirectionsIcon from '@material-ui/icons/Directions'
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalk'
import DirectionsBikeIcon from '@material-ui/icons/DirectionsBike'
import RoomIcon from '@material-ui/icons/Room'
import ServicesTabs from '../component/ServicesTabs'
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import EmailIcon from '@material-ui/icons/Email';
import Featured from '../component/Featured'
import HeureService from '../component/HeureService';


export default function DetailSingle() {
  return (
    <div>
      <br/> <div className="dvider-bar"></div>
      <div>
        <div className="p-2 mt-2">
          <div className="descript">
            <img src="images/vancouver.jpg" style={{width:'20rem'}} alt='logo'/>
            <div class="descript-img">
              <img src="../images/logo.png"/>
            </div>
          </div> <br/>
          <p className='boutTitle mb-0'>Centre Baha'i<MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginLeft:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/></p>
          <p className='mb-1'>Restaurant </p>
          <h5 style={{color:'gray'}}>Ouvert jusqu'à 24h30 </h5>
          <ul className='dirct'>
            <li><a href='#' className='btn btn-principal'><DirectionsIcon style={{fontSize:'20px', color:'white', textAlign:'center', margin:'3px'}}/>directions</a></li>
            <li><a href='#' className='btn btn-second p-1'><PublicIcon style={{fontSize:'40px', color:'#b67f19', textAlign:'center', }}/></a></li>
            <li><a href='#' className='btn btn-second p-1'><MapIcon style={{fontSize:'40px', color:'#b67f19', textAlign:'center',}}/></a></li>
          </ul>
      
        </div>


        <div className="mapBox" id="mapmobile">
          <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d15889.775207839735!2d-4.0144876522319874!3d5.349031875836721!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1scit%C3%A9%20des%20arts!5e0!3m2!1sfr!2sci!4v1623243380144!5m2!1sfr!2sci" width="100%" height="100%" style={{border:'0'}} allowfullscreen="" loading="lazy"></iframe>
        </div> <br/>
        <div className="dvider-bar"></div>

        <div className="p-4">
            <h3 className="spons">Services</h3>
            <div className="back_gray p-2">
              <ServicesTabs/>
            </div>

            <h4 className="mt-4">Photos</h4>
            <div className="back_gray p-4" style={{height:'30rem'}}>
              <Featured/>
            </div>
          </div>
        <div className="dvider-bar"></div> <br/>
        

        <div className="p-4 boutCard">
          <h2>Contact</h2>
            <ul>
              <li className="mb-2"><RoomIcon style={{color:'#ff7600'}}/> Cocody, Abidjan, Côte d'ivoire</li>
              <li className="mb-2"> <PhoneIphoneIcon style={{color:'#ff7600'}}/> +225 01 02 03 04 05</li>
              <li className="mb-2"> <EmailIcon style={{color:'#ff7600'}}/> boutique@gmail.com</li>
            </ul>
        </div> <br/>

        <div className="dvider-bar"></div>  <br/>
          
          <div className="destination-val">
            <div style={{width:'70%', backgroundColor:'rgba(255, 255, 255, 0.685)', margin:'auto', padding:'10px', borderRadius:'10px'}}>
              <h4>Choisissez où manger, quoi voir et comment vous amuser</h4>
            </div>
          </div>
          <br/>
        <div className="dvider-bar"></div> <br/>

        <section style={{padding:'4px', marginTop:'30px', marginBottom:'30px'}}>
          <HeureService/>
        </section>

        <div className="p-4 boutCard mb-55">
          <h3>Direction</h3>
          <div style={{padding:'20px', backgroundColor:'#f9f9f9'}} className="margin-bottom-30">
          <div className="row">
            <div className="col-3"></div>
            <div className="col-2">
              <div className="icon_direct">
                <li><a href="#"><DirectionsCarIcon style={{ fontSize: 30 }} /></a></li>
              </div>
            </div>
            <div className="col-2">
              <div className="icon_direct">
                <li><a href="#"><DirectionsWalkIcon style={{ fontSize: 30 }} /></a></li>
              </div>
            </div>
            <div className="col-2">
              <div className="icon_direct">
                <li><a href="#"><DirectionsBikeIcon style={{ fontSize: 30 }} /></a></li>
              </div>
            </div>
            <div className="col-3"></div>
          </div> <br/>
          <div className="row">
            <div className="col-lg-1 m-auto">
              <div style={{height:'15px',border:'2px solid black', width:'15px',borderRadius:'50%', backgroundColor:'#54ba1d',margin:'auto', position: 'absolute',right:'0',bottom: '0px'}}></div>
            </div>
            <div className="col-lg-10">
                <input type="text" name="nom" placeholder="Veuillez saisir votre Position Actuelle" />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-1 m-auto">
              <div style={{height:'15px',border:'2px solid black', width:'15px',borderRadius:'50%', backgroundColor:'#f98925',margin:'auto', position: 'absolute',right:'0',bottom: '0px'}}></div>
            </div>
          <div className="col-lg-10">
              <input type="text" name="lieu" placeholder="Veuillez saisir votre Lieu choisi" />
          </div>
          </div>

          <div>
            <p style={{fontSize:'18px'}}>Temps estimé à : <span style={{color: '#54ba1d',fontWeight:'bold'}}>03heure 30minute</span></p>
          </div>

        </div>
        </div>
        
      </div>
    </div>
  )
}
