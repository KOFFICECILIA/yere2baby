import MapIcon from '@material-ui/icons/Map';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera'
import PublicIcon from '@material-ui/icons/Public'
import DirectionsIcon from '@material-ui/icons/Directions'
import Link from 'next/link'
import Image from 'next/image'
const link = '/Boutiques/' 


export async function getStaticProps() {
  
  const res = await fetch('http://51.68.44.231:1998/api/v1/domaines')
  const data = await res.json()


  return {
    props: {
      data:data, 
    },
  }
}

export default function Home({data}) {
  return (
    <div style={{padding:'20px 4px'}}>
            <div className="category-box">
            
              <ul>
              {data.map((domaines) => (
                
                <li key={domaines.id}>
                  <div className="text-center">
                      <Link href={link + domaines.id}>
                        <a>
                          <div className="cat-img">
                            <Image src={domaines.icon} height="32" width="32" />
                          </div>
                          <p>{domaines.domaine}</p>
                        </a>
                      </Link>
                  </div>
                </li>
         
              ))}
              </ul>
               <br/>
                
              </div>
              <div className="mapBox" id="mapmobile">
          <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d15889.775207839735!2d-4.0144876522319874!3d5.349031875836721!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1scit%C3%A9%20des%20arts!5e0!3m2!1sfr!2sci!4v1623243380144!5m2!1sfr!2sci" width="100%" height="100%" style={{border:'0'}} allowfullscreen="" loading="lazy"></iframe>
        </div> <br/>
        </div>
  )
}

