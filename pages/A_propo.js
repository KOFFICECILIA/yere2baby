import MapIcon from '@material-ui/icons/Map';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera'
import PublicIcon from '@material-ui/icons/Public'
import DirectionsIcon from '@material-ui/icons/Directions'
import NaturePeopleIcon from '@material-ui/icons/NaturePeople'
import RoomIcon from '@material-ui/icons/Room'


export default function A_propo() {
  return (
    <div>
      <div>
        <div className="p-4">
          <h2>À propos de Yéré2Baby</h2>
          <p>Parcourez Abidjan avec Mapcarta, la carte ouverte. Yéré2Baby est le moyen le plus simple d'explorer les differents endroits pour s'epannuir. Votre monde est sans frontières. </p>
          <h5 className="spons">yèrè2babi@gmail.com</h5>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-3 text-center">
              <div className="category_small_box_one">
                <a href="/Maps">
                  <MapIcon style={{fontSize:'36', color:'white', textAlign:'center', marginTop:'10px'}}/>
                </a>
              </div>
              <p>Maps</p>
            </div>
            <div className="col-3 text-center">
              <div className="category_small_box_two">
                <a href="#">
                  <PhotoCameraIcon style={{fontSize:'36px', color:'white', textAlign:'center', marginTop:'10px'}}/>
                </a>
              </div>
              <p>Photo Maps</p>
            </div>
            <div className="col-3 text-center">
              <div className="category_small_box_three">
                <a href="#">
                  <PublicIcon style={{fontSize:'36px', color:'white', textAlign:'center', marginTop:'10px'}}/>
                </a>
              </div>
              <p>Satelite</p>
            </div>
            <div className="col-3 text-center">
              <div className="category_small_box_four">
                <a href="#">
                  <DirectionsIcon style={{fontSize:'36px', color:'white', textAlign:'center', marginTop:'10px'}}/>
                </a>
              </div>
              <p>Direction</p>
            </div>
          </div><br/>
          
        </div>


        <div className="mapBox" id="mapmobile">
          <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d15889.775207839735!2d-4.0144876522319874!3d5.349031875836721!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1scit%C3%A9%20des%20arts!5e0!3m2!1sfr!2sci!4v1623243380144!5m2!1sfr!2sci" width="100%" height="100%" style={{border:'0'}} allowfullscreen="" loading="lazy"></iframe>
        </div> <br/>
        <div className="dvider-bar"></div>
        

        <div className="p-4">
          <h2>Merci</h2>
          <p>Un grand merci aux contributeurs de OpenStreetMap, Wikidata, GeoNames, Wikimedia Commons, Wikipedia et Wikivoyage. Merci à Mapbox pour fourni des cartes incroyables.</p>
        </div> <br/>
        <div className="dvider-bar"></div> 
          <div className="p-4">
            <h3>Politique de confidentialité</h3>
            <p>Ce site utilise des cookies afin d'afficher des annonces de Clicktripz, Google et d'autres sociétés. Consultez la politique de confidentialité de Clicktripz et aboutads.info. Vous pouvez désactiver les cookies publicitaires de Google.</p>
            <p>Ce site utilise des cookies afin d'afficher des annonces de Clicktripz, Google et d'autres sociétés. Consultez la politique de confidentialité de Clicktripz et aboutads.info. Vous pouvez désactiver les cookies publicitaires de Google.</p>
          </div>

    
        <div className="dvider-bar"></div> <br/>
          <div className="destination-val">
            <div style={{width:'70%', backgroundColor:'rgba(255, 255, 255, 0.685)', margin:'auto', padding:'10px', borderRadius:'10px'}}>
              <h4>Choisissez où manger, quoi voir et comment vous amuser</h4>
            </div>
          </div>
          <br/>
        <div className="dvider-bar"></div> <br/>
        <div className="p-4">
            <h3>Autres lieux</h3>
            <div className="row">
              <div className="col-6 mb-4">
                <a href="/DetailSingle">
                  <div className="suggest suggest-back_color-one">Bijouterie</div>
                </a>
              </div>
              <div className="col-6 mb-4">
                <a href="/DetailSingle">
                  <div className="suggest suggest-back_color-two">Restaurant</div>
                </a>
              </div>
              <div className="col-6 mb-4">
                <a href="/DetailSingle">
                  <div className="suggest suggest-back_color-three">Cordonniers</div>
                </a>
              </div>
              <div className="col-6 mb-4">
                <a href="/DetailSingle">
                  <div className="suggest suggest-back_color-four">Boutiques Mode</div>
                </a>
              </div>
            </div>
          </div>

        <section style={{padding:'4px', marginTop:'30px'}}>
          <div className="text-center">
            <img src="images/logo.png" style={{height:'30px', margin:'10px', borderRadius:'4px'}} alt='logo'/>
            <img src="images/logo.png" style={{height:'30px', margin:'10px', borderRadius:'4px'}} alt='logo'/>
            <img src="images/logo.png" style={{height:'30px', margin:'10px', borderRadius:'4px'}} alt='logo'/>
          </div>
        </section>
      </div>
    </div>
  )
}
