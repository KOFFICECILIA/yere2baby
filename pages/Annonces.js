import Head from 'next/head'
import Link from 'next/link'
import MapIcon from '@material-ui/icons/Map';
import fetch from 'isomorphic-unfetch'

function Blog({ data }) {
  console.log(data);
  return (
    <div className="category-box">
      <ul>
      {data.map((dom) => (
        <li>
          <div className="text-center">
            <a href="/Boutiques">
              <MapIcon style={{fontSize:'50', borderRadius:'50%', padding:'10px', color:'white', backgroundColor:'#ffa15e', textAlign:'center'}}/>
              <p>{dom.domaine}</p>
            </a>
          </div>
        </li>
  
        // <div>
        //   <h3>{ninja.name}</h3>
        //   <p>{ninja.username}</p>
        // </div> 
      ))}
    </ul>
    
    </div>
  )
}

export async function getStaticProps() {
  
  const res = await fetch('http://51.68.44.231:1998/api/v1/domaines')
  const data = await res.json()

  
  return {
    props: {
      data, 
    },
  }
}


export default Blog
