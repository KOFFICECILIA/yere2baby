import MapIcon from '@material-ui/icons/Map';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar'
import PublicIcon from '@material-ui/icons/Public'
import DirectionsIcon from '@material-ui/icons/Directions'
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalk'
import DirectionsBikeIcon from '@material-ui/icons/DirectionsBike'
import RoomIcon from '@material-ui/icons/Room'
import ServicesTabs from '../../component/ServicesTabs'
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import EmailIcon from '@material-ui/icons/Email';
import Featured from '../../component/Featured'
import HeureService from '../../component/HeureService';
import fetch from 'isomorphic-unfetch'


export async function getStaticPaths() {
  
  const res = await fetch('http://51.68.44.231:1998/api/v1/boutiques')
  const data = await res.json()
 
  const paths = data.map(Boutique =>{
    return{
      params:{id: Boutique.id.toString(),}
    }
  })

  return {
    paths,
    fallback:false
  }
}


export async function getStaticProps(context) {
  const id = context.params.id;
  const res = await fetch('http://51.68.44.231:1998/api/v1/boutiques/' + id)
  const data = await res.json()

  return{
    props:{Boutique: data}
  }
}

export default function DetailSingle({boutique}) {
  return (
    <div>
      <h1>{boutique.nom}</h1>
    </div>
  )
}
