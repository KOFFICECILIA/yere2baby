import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(25),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function SimpleAccordion() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Accordion style={{borderRadius:'0', backgroundColor:'transparent', color:'#000'}} className="sidebar-user-material-footer">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          className="d-flex justify-content-between align-items-center text-shadow-dark"
        >
          <Typography className={classes.heading}>Heures de travail</Typography>
        </AccordionSummary>
        <AccordionDetails style={{backgroundColor:'#fff', padding:'25px 50px'}}>
          <div style={{width: '100%'}}>
                <ul>
                    <li className="" style={{display: 'flex', justifyContent:'space-between', width:'100%'}}>
                        <h5>Lundi</h5>
                        <p className="">08h-23h</p>
                    </li>
                    <li className="" style={{display: 'flex', justifyContent:'space-between', width:'100%'}}>
                        <h5>Mardi</h5>
                        <p className="">08h-23h</p>
                    </li>
                    <li className="" style={{display: 'flex', justifyContent:'space-between', width:'100%'}}>
                        <h5>Mercredi</h5>
                        <p className="">08h-23h</p>
                    </li>
                    <li className="" style={{display: 'flex', justifyContent:'space-between', width:'100%'}}>
                        <h5>Jeudi</h5>
                        <p className="">08h-23h</p>
                    </li>
                    <li className="" style={{display: 'flex', justifyContent:'space-between', width:'100%'}}>
                        <h5>Vendredi</h5>
                        <p className="">08h-23h</p>
                    </li>
                    <li className="" style={{display: 'flex', justifyContent:'space-between', width:'100%'}}>
                        <h5>Samedi</h5>
                        <p className="">08h-23h</p>
                    </li>
                    <li className="" style={{display: 'flex', justifyContent:'space-between', width:'100%'}}>
                        <h5>Dimanche</h5>
                        <p className="">08h-23h</p>
                    </li>
                    
                </ul>
            </div>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
