import React from 'react'

export default function Header(){
    return(
        <div>
            <div className="main_inner_search_block p-4">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                        <div className="main_input_search_part">
                            <div className="main_input_search_part_item">
                            <input type="text" placeholder="Que rechercher-vous ?" value=""/>
                            </div>
                            <button className="button">Recherche</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}