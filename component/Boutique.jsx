import React from 'react'
import MapIcon from '@material-ui/icons/Map';

export default function Boutique(){
    return(
        <div style={{paddingTop:'7px'}}>
            <div className="">
              <ul>
                <li className="boutCard">
                  <a href="/DetailSingle">
                    <p className='boutTitle'>Centre Baha'i <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginLeft:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/></p>
                      <div className="descript">
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt magna aliqua. 
                          Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                        </p>
                        <div class="descript-img">
                          <img src="../images/logo.png"/>
                        </div>
                      </div>
                      <h6 style={{color: '#65b619'}}> 
                        <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginRight:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/>
                         Lorem ipsum dolor sit amet, sed do eiusmod
                      </h6>
                    <h5>Ouvert jusqu'à 24h30 </h5>
                  </a>
                </li>
                <li className="boutCard">
                  <a href="/DetailSingle">
                    <p className='boutTitle'>Centre Baha'i <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginLeft:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/></p>
                      <div className="descript">
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt magna aliqua. 
                          Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                        </p>
                        <div class="descript-img">
                          <img src="../images/logo.png"/>
                        </div>
                      </div>
                      <h6 style={{color: '#65b619'}}> 
                        <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginRight:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/>
                         Lorem ipsum dolor sit amet, sed do eiusmod
                      </h6>
                    <h5>Ouvert jusqu'à 24h30 </h5>
                  </a>
                </li>
                <li className="boutCard">
                  <a href="/DetailSingle">
                    <p className='boutTitle'>Centre Baha'i <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginLeft:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/></p>
                      <div className="descript">
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt magna aliqua. 
                          Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                        </p>
                        <div class="descript-img">
                          <img src="../images/logo.png"/>
                        </div>
                      </div>
                      <h6 style={{color: '#65b619'}}> 
                        <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginRight:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/>
                         Lorem ipsum dolor sit amet, sed do eiusmod
                      </h6>
                    <h5>Ouvert jusqu'à 24h30 </h5>
                  </a>
                </li>
                <li className="boutCard">
                  <a href="/DetailSingle">
                    <p className='boutTitle'>Centre Baha'i <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginLeft:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/></p>
                      <div className="descript">
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt magna aliqua. 
                          Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                        </p>
                        <div class="descript-img">
                          <img src="../images/logo.png"/>
                        </div>
                      </div>
                      <h6 style={{color: '#65b619'}}> 
                        <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginRight:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/>
                         Lorem ipsum dolor sit amet, sed do eiusmod
                      </h6>
                    <h5>Ouvert jusqu'à 24h30 </h5>
                  </a>
                </li>
              </ul>
               <br/>
                
              </div>
              <div className="mapBox" id="mapmobile">
          <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d15889.775207839735!2d-4.0144876522319874!3d5.349031875836721!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1scit%C3%A9%20des%20arts!5e0!3m2!1sfr!2sci!4v1623243380144!5m2!1sfr!2sci" width="100%" height="100%" style={{border:'0'}} allowfullscreen="" loading="lazy"></iframe>
        </div> <br/>
        </div>
    )
}