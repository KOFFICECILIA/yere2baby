import React from 'react'

export default function MapsBox(){
    return(
        <div id="mapbox">
            <div className="mapBox">
              <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d15889.775207839735!2d-4.0144876522319874!3d5.349031875836721!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1scit%C3%A9%20des%20arts!5e0!3m2!1sfr!2sci!4v1623243380144!5m2!1sfr!2sci" width="100%" height="100%" style={{border:'0'}} allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    )
}