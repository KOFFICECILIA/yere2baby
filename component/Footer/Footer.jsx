import React from 'react'

export default function Footer(){
    return(
        <div>
            <footer className="footer p-4 text-center">
                <img src="images/yèrè2babi.png" style={{height:'80px', borderRadius:'4px', marginBottom:'5px'}} alt='logo'/>
                <p> <a href="/A_propo" style={{color:'#ff7600'}}>About yèrè2babi</a> . Thanks to Mapbox for providing amazing maps. Data © OpenStreetMap contributors and available under the Open Database License. Text is available under the CC BY-SA 4.0 license, excluding photos, directions and the map. Photo: Wikimedia, CC BY 2.5.</p>
            </footer>
        </div>
    )
}